package decripciones;

public class Acura extends Vehiculo implements IConcesionario {

	private IConcesionario sucesor = new Vehiculo();
	private final int descripcionBasica = 2;

	public void getDescripcion(int tipoDescripcion) {
		
		if(tipoDescripcion == descripcionBasica) {

			System.out.println("---------------------------");
			System.out.println("//  Acura                //");
			System.out.println("//  Bajo perfil          //");
			System.out.println("//  Autos prestijiosos   //");
			System.out.println("---------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}	
}
