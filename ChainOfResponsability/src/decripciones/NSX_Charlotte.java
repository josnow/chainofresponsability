package decripciones;

public class NSX_Charlotte implements IConcesionario {

	private IConcesionario sucesor = new CoupeAcura();
	private final int descripcionProfunda = 4;
	@Override
	public void getDescripcion(int tipoDescripcion) {

		if(tipoDescripcion == descripcionProfunda) {
			System.out.println("----------------------------------------");
			System.out.println("//  Cupe  -  Acura  -  NSX Charlotte  //");
			System.out.println("//        Alto rendimiento            //");
			System.out.println("//         Auto increible             //");
			System.out.println("----------------------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

}
