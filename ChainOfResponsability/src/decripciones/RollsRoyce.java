package decripciones;

public class RollsRoyce extends Vehiculo implements IConcesionario {

	private IConcesionario sucesor = new Vehiculo();
	private final int descripcionBasica = 2;
	@Override
	public void getDescripcion(int tipoDescripcion) {
		
		if(tipoDescripcion == descripcionBasica) {
		
		System.out.println("---------------------------");
		System.out.println("//  RollsRoyce            //");
		System.out.println("//  Altos costos          //");
		System.out.println("//  Modelos muy elegantes //");
		System.out.println("---------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

	
}
