package decripciones;

public class Panamera implements IConcesionario {

	private IConcesionario sucesor = new CamionetaPorshe();
	private final int descripcionProfunda = 4;
	@Override
	public void getDescripcion(int tipoDescripcion) {

		if(tipoDescripcion == descripcionProfunda) {
			System.out.println("----------------------------------------");
			System.out.println("//  Camioneta  -  Porshe  -  Panamera  //");
			System.out.println("//        Motor muy suave              //");
			System.out.println("//         Gran potencia               //");
			System.out.println("----------------------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

}
