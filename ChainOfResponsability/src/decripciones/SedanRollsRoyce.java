package decripciones;

public class SedanRollsRoyce implements IConcesionario {

	private IConcesionario sucesor = new RollsRoyce();
	private final int descripcionMedia = 3;
	@Override
	public void getDescripcion(int tipoDescripcion) {

		if(tipoDescripcion == descripcionMedia) {
			System.out.println("---------------------------");
			System.out.println("//  Sedan  -  RollsRoyce      //");
			System.out.println("//  Bajo consumo     //");
			System.out.println("//  Autos deportivos        //");
			System.out.println("---------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

}
