package decripciones;

public class Porshe extends Vehiculo implements IConcesionario {

	private IConcesionario sucesor = new Vehiculo();
	private final int descripcionBasica = 2;

	@Override
	public void getDescripcion(int tipoDescripcion) {
		
		if(tipoDescripcion == descripcionBasica) {

		System.out.println("---------------------------");
		System.out.println("//  Porshe               //");
		System.out.println("//  Altas velocidades    //");
		System.out.println("//  Estilos deportivos   //");
		System.out.println("---------------------------");
		
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}
}
