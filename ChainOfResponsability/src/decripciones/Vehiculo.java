package decripciones;

public class Vehiculo implements IConcesionario {
	
	@Override
	public void getDescripcion(int tipoDescripcion) {
		
		System.out.println("---------------------------");
		System.out.println("//  Vehiculo             //");
		System.out.println("//  Tiene ruedas         //");
		System.out.println("//  Transporte mecanico  //");
		System.out.println("---------------------------");

	}
}
