package decripciones;

public class CoupeAcura implements IConcesionario {

	private IConcesionario sucesor = new Acura();
	private final int descripcionMedia = 3;
	@Override
	public void getDescripcion(int tipoDescripcion) {

		if(tipoDescripcion == descripcionMedia) {
			System.out.println("---------------------------");
			System.out.println("//  Cupe  -  Acura      //");
			System.out.println("//  Bajo rendimiento     //");
			System.out.println("//  Autos lujosos        //");
			System.out.println("---------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

}
