package main;

import decripciones.*;

public class User {

	private final static int descripcionGeneral = 1;
	private final static int descripcionBasica = 2;
	private final static int descripcionMedia = 3;
	private final static int descripcionProfunda = 4;

	public static void main(String[] args) {

		IConcesionario vehiculo;
		
		vehiculo = new NSX_Charlotte();
		vehiculo.getDescripcion(descripcionProfunda);
		vehiculo.getDescripcion(descripcionBasica);
		
		vehiculo = new Phantom();
		vehiculo.getDescripcion(descripcionGeneral);
		vehiculo.getDescripcion(descripcionBasica);
		vehiculo.getDescripcion(descripcionMedia);
		vehiculo.getDescripcion(descripcionProfunda);
		
		vehiculo = new Panamera();
		vehiculo.getDescripcion(descripcionGeneral);
		vehiculo.getDescripcion(descripcionBasica);
		vehiculo.getDescripcion(descripcionMedia);
		vehiculo.getDescripcion(descripcionProfunda);

	}

	
}
